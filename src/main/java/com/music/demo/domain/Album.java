package com.music.demo.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "ALBUMS")
public class Album extends ExtendedBaseEntity<Long> {

    @Column(name = "NAME", nullable = false)
    private String name;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "GENRE_ID", unique = true, nullable = false)
    private Genre genre;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "album")
    private List<Track> tracks;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public List<Track> getTracks() {
        return tracks;
    }

    public void setTracks(List<Track> tracks) {
        this.tracks = tracks;
    }
}
