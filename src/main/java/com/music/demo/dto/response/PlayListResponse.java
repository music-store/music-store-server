package com.music.demo.dto.response;

import java.util.List;

public class PlayListResponse {
    private Long id;
    private String name;
    private Integer userId;
    private List<Long> trackIds;

    public PlayListResponse(Long id, String name, Integer userId, List<Long> trackIds) {
        this.id = id;
        this.name = name;
        this.userId = userId;
        this.trackIds = trackIds;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public List<Long> getTrackIds() {
        return trackIds;
    }

    public void setTrackIds(List<Long> trackIds) {
        this.trackIds = trackIds;
    }
}
