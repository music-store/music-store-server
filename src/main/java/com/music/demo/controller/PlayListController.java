package com.music.demo.controller;

import com.music.demo.domain.PlayList;
import com.music.demo.domain.Track;
import com.music.demo.dto.request.PlayListDtoRequest;
import com.music.demo.dto.response.PlayListResponse;
import com.music.demo.security.SecurityUser;
import com.music.demo.service.PlayListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("${web.prefix}/playlists")
public class PlayListController {

    @Autowired
    private PlayListService playListService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public PlayListResponse getById(@PathVariable("id") Long id) {
        return build(playListService.getById(id));
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<PlayListResponse> getList(@AuthenticationPrincipal SecurityUser securityUser) {
        return this.buildList(playListService.getList(securityUser.getUser().getId()));
    }

    @RequestMapping(method = RequestMethod.POST)
    public PlayListResponse create(@AuthenticationPrincipal SecurityUser securityUser,
                                   @RequestBody PlayListDtoRequest playListDtoRequest) {
        playListDtoRequest.setUserId(securityUser.getUser().getId());
        return build(playListService.insert(playListDtoRequest));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public PlayListResponse update(@PathVariable("id") Long id,
                                   @RequestBody PlayListDtoRequest playListDtoRequest) {
        playListDtoRequest.setId(id);

        return build(playListService.update(playListDtoRequest));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") Long id) {
        playListService.delete(id);
    }

    private PlayListResponse build(PlayList p) {
        return new PlayListResponse(
                p.getId(),
                p.getName(),
                p.getUser().getId(),
                p.getTracks().stream().map(Track::getId).collect(Collectors.toList())
        );
    }

    private List<PlayListResponse> buildList(List<PlayList> list) {
        List<PlayListResponse> responseList = new ArrayList<>(list.size());
        list.forEach((p) -> responseList.add(build(p)));

        return responseList;
    }
}

