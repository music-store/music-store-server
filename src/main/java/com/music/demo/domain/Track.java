package com.music.demo.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "TRACKS")
public class Track extends ExtendedBaseEntity<Long> {

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "DURATION", nullable = false)
    private Integer duration;

    @Column(name = "URL", nullable = false)
    private String url;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Album.class)
    @JoinColumn(name = "ALBUM_ID", nullable = false, referencedColumnName = "id")
    private Album album;

    @ManyToMany(fetch = FetchType.LAZY)
    private List<PlayList> playLists = new ArrayList<>();

    public Track() {

    }

    public Track(String name, Integer duration, String url) {
        this.name = name;
        this.duration = duration;
        this.url = url;
    }

    public List<PlayList> getPlayLists() {
        return playLists;
    }

    public void setPlayLists(List<PlayList> playLists) {
        this.playLists = playLists;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
