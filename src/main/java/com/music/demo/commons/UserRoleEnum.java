package com.music.demo.commons;

public enum UserRoleEnum {
    ADMIN,
    GUEST,
    AUTHOR;

    UserRoleEnum() {
    }
}
