package com.music.demo;

import com.music.demo.domain.Genre;
import com.music.demo.domain.User;
import com.music.demo.repository.GenreRepository;
import com.music.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class DatabaseLoader implements CommandLineRunner {
    private final String[] genreNames = {"Рок", "Поп", "Рэп"};

    private final GenreRepository genreRepository;
    private final UserRepository userRepository;

    @Autowired
    public DatabaseLoader(GenreRepository _genre, UserRepository _user) {
        this.genreRepository = _genre;
        this.userRepository = _user;
    }

    @Override
    public void run(String... strings) throws Exception {
        this.generateGenres();
        this.generateUsers();
    }

    private void generateGenres() throws Exception {
        ArrayList<Genre> genres = new ArrayList<>();

        for (String genreName : this.genreNames) {
            Genre genre = new Genre();
            genre.setName(genreName);
            genres.add(genre);
        }

        this.genreRepository.saveAll(genres);
    }

    private void generateUsers() throws Exception {
        User user = new User();
        user.setName("Andrey");
        user.setLogin("vofus");
        user.setPassword("123456");

        User user2 = new User();
        user2.setName("Test");
        user2.setLogin("test");
        user2.setPassword("123456");

        this.userRepository.save(user);
        this.userRepository.save(user2);
    }
}
