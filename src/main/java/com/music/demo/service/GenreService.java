package com.music.demo.service;

import com.music.demo.domain.Genre;
import com.music.demo.repository.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Objects;


@Service
public class GenreService {

    @Autowired
    private GenreRepository genreRepository;

    public List<Genre> getList(Integer id) {
        if (!Objects.isNull(id)) {
            Genre genre = genreRepository.findById(id).orElseThrow(() -> new RuntimeException("genre does not exist"));
            return Collections.singletonList(genre);
        }
        return (List<Genre>) genreRepository.findAll();
    }
}
