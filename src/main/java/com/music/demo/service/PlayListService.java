package com.music.demo.service;

import com.music.demo.domain.PlayList;
import com.music.demo.domain.Track;
import com.music.demo.domain.User;
import com.music.demo.dto.request.PlayListDtoRequest;
import com.music.demo.repository.PlayListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlayListService {

    @Autowired
    private PlayListRepository playListRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private TrackService trackService;

    public PlayList getById(Long id) {
        return playListRepository.findById(id).orElseThrow(() -> new RuntimeException("Playlist does not exist"));
    }

    public List<PlayList> getList(Integer userId) {
        return playListRepository.findAllByUserId(userId);
    }

    public PlayList insert(PlayListDtoRequest playListDtoRequest) {
        User user = userService.getList(playListDtoRequest.getUserId()).get(0);
        List<Track> tracks = trackService.getList(playListDtoRequest.getTrackIds(), null);

        PlayList playList = new PlayList();
        playList.setUser(user);
        playList.setName(playListDtoRequest.getName());
        playList.setTracks(tracks);

        return playListRepository.save(playList);
    }

    public PlayList update(PlayListDtoRequest playListRequest) {
        PlayList old = playListRepository
                .findById(playListRequest.getId())
                .orElseThrow(() -> new RuntimeException("Playlist does not exist"));

        List<Track> tracks = trackService.getList(playListRequest.getTrackIds(), null);
        PlayList playList = new PlayList();
        playList.setId(old.getId());
        playList.setName(playListRequest.getName());
        playList.setUser(old.getUser());
        playList.setTracks(tracks);

        return playListRepository.save(playList);
    }

    public void delete(Long id) {
        playListRepository.deleteById(id);
    }
}
