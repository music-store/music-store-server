package com.music.demo.repository;

import com.music.demo.domain.Album;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface AlbumRepository extends PagingAndSortingRepository<Album, Long> {

}
