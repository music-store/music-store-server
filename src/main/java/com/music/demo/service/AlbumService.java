package com.music.demo.service;

import com.music.demo.domain.Album;
import com.music.demo.domain.Genre;
import com.music.demo.dto.request.AlbumDtoRequest;
import com.music.demo.repository.AlbumRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AlbumService {

    @Autowired
    private AlbumRepository albumRepository;

    @Autowired
    private GenreService genreService;

    @Autowired
    private TrackService trackService;

    public List<Album> getList() {
        return (List<Album>) albumRepository.findAll();
    }

    public Album getById(Long id) {
        return albumRepository.findById(id).orElseThrow(() -> new RuntimeException("Album does not exist"));
    }

    public Album update(AlbumDtoRequest albumDtoRequest) {
        Album oldAlbum = albumRepository
                .findById(albumDtoRequest.getId())
                .orElseThrow(() -> new RuntimeException("Album does not exist"));

        Album album = new Album();
        album.setId(oldAlbum.getId());
        album.setName(albumDtoRequest.getName());
        album.setTracks(oldAlbum.getTracks());

        if (!oldAlbum.getGenre().getId().equals(albumDtoRequest.getGenreId())) {
            Genre genre = genreService.getList(albumDtoRequest.getGenreId()).get(0);
            album.setGenre(genre);
        } else {
            album.setGenre(oldAlbum.getGenre());
        }

        return albumRepository.save(album);
    }

    @Transactional
    public Album insert(AlbumDtoRequest albumDtoRequest) {
        Album album = new Album();
        album.setName(albumDtoRequest.getName());
        album.setGenre(genreService.getList(albumDtoRequest.getGenreId()).get(0));

        return albumRepository.save(album);
    }

    @Transactional
    public void delete(Long id) {
        trackService.deleteByAlbumId(id);
        albumRepository.deleteById(id);
    }
}
