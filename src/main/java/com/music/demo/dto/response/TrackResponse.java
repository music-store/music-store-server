package com.music.demo.dto.response;

public class TrackResponse {
    private Long id;
    private String name;
    private Integer duration;
    private String url;
    private Long albumId;

    public TrackResponse(Long id, String name, Integer duration, String url, Long albumId) {
        this.id = id;
        this.name = name;
        this.duration = duration;
        this.url = url;
        this.albumId = albumId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getAlbumId() {
        return albumId;
    }

    public void setAlbumId(Long albumId) {
        this.albumId = albumId;
    }
}
