package com.music.demo.service;

import com.music.demo.domain.Album;
import com.music.demo.domain.PlayList;
import com.music.demo.domain.Track;
import com.music.demo.dto.request.TrackDtoRequest;
import com.music.demo.repository.PlayListRepository;
import com.music.demo.repository.TrackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TrackService {

    @Autowired
    private TrackRepository trackRepository;
    @Autowired
    private AlbumService albumService;
    @Autowired
    private PlayListRepository playListRepository;

    public List<Track> getList(List<Long> ids, Long albumId) {
        if (!CollectionUtils.isEmpty(ids)) {
            return (List<Track>) trackRepository.findAllById(ids);
        }

        return trackRepository.findAllByAlbum_Id(albumId);
    }

    public Track update(TrackDtoRequest trackRequest) {
        Track oldTrack = trackRepository
                .findById(trackRequest.getId())
                .orElseThrow(() -> new RuntimeException("Track does not exist"));
        Track track = new Track(oldTrack.getName(), oldTrack.getDuration(), oldTrack.getUrl());

        return trackRepository.save(track);
    }

    public Track insert(TrackDtoRequest trackRequest) {
        Track track = new Track(trackRequest.getName(), trackRequest.getDuration(), trackRequest.getUrl());
        Album album = albumService.getById(trackRequest.getAlbumId());
        track.setAlbum(album);

        return trackRepository.save(track);
    }

    public void deleteByAlbumId(Long albumId) {
        trackRepository.deleteAllByAlbum_Id(albumId);
    }

    public void delete(Long id) {
        Track track = trackRepository
                .findById(id)
                .orElseThrow(() -> new RuntimeException("Track does not exist"));

        List<PlayList> playlists = playListRepository.findAllByTracksIsContaining(track);
        playlists.forEach((p) -> {
            List<Track> tracks = p.getTracks().stream().filter((t) -> !t.getId().equals(id)).collect(Collectors.toList());
            p.setTracks(tracks);
            playListRepository.save(p);
        });

        trackRepository.deleteById(id);
    }
}
