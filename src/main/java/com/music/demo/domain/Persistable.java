package com.music.demo.domain;

import java.io.Serializable;

public interface Persistable<T> extends Serializable, Cloneable {
    T getId();
}
