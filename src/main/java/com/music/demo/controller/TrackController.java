package com.music.demo.controller;

import com.music.demo.domain.Track;
import com.music.demo.dto.request.TrackDtoRequest;
import com.music.demo.dto.response.TrackResponse;
import com.music.demo.service.TrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("${web.prefix}/tracks")
public class TrackController {

    @Autowired
    private TrackService trackService;

    @RequestMapping(method = RequestMethod.GET)
    public List<TrackResponse> getList(@RequestParam(name = "ids", required = false) List<Long> ids,
                                       @RequestParam(name = "album_id", required = false) Long albumId) {
        return buildList(trackService.getList(ids, albumId));
    }

    @RequestMapping(method = RequestMethod.POST)
    public TrackResponse create(@RequestBody TrackDtoRequest trackRequest) {
        Track track = trackService.insert(trackRequest);
        return build(track);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public TrackResponse update(@PathVariable("id") Long id,
                                @RequestBody TrackDtoRequest trackRequest) {
        trackRequest.setId(id);
        Track track = trackService.update(trackRequest);
        return build(track);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") Long id) {
        trackService.delete(id);
    }

    private TrackResponse build(Track t) {
        return new TrackResponse(
                t.getId(),
                t.getName(),
                t.getDuration(),
                t.getUrl(),
                t.getAlbum().getId()
        );
    }

    private List<TrackResponse> buildList(List<Track> list) {
        List<TrackResponse> responseList = new ArrayList<>(list.size());
        list.forEach((a) -> responseList.add(build(a)));

        return responseList;
    }
}

