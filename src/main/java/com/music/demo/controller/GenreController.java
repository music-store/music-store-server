package com.music.demo.controller;

import com.music.demo.domain.Genre;
import com.music.demo.dto.response.GenreResponse;
import com.music.demo.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("${web.prefix}/genres")
public class GenreController {

    @Autowired
    private GenreService genreService;


    @RequestMapping(method = RequestMethod.GET)
    public List<GenreResponse> getList(@RequestParam(name = "id", required = false) Integer genreId) {
        return buildList(genreService.getList(genreId));
    }

    private GenreResponse build(Genre g) {
        return new GenreResponse(g.getId(), g.getName());
    }

    private List<GenreResponse> buildList(List<Genre> list) {
        List<GenreResponse> responseList = new ArrayList<>(list.size());
        list.forEach((a) -> responseList.add(build(a)));

        return responseList;
    }
}

