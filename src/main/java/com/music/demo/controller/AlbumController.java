package com.music.demo.controller;

import com.music.demo.domain.Album;
import com.music.demo.domain.Track;
import com.music.demo.dto.request.AlbumDtoRequest;
import com.music.demo.dto.response.AlbumResponse;
import com.music.demo.service.AlbumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("${web.prefix}/albums")
public class AlbumController {

    @Autowired
    private AlbumService albumService;


    @RequestMapping(method = RequestMethod.GET)
    public List<AlbumResponse> getList() {
        return buildList(albumService.getList());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public AlbumResponse getById(@PathVariable("id") Long id) {
        return build(albumService.getById(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    public AlbumResponse create(@RequestBody AlbumDtoRequest albumDtoRequest) {
        return build(albumService.insert(albumDtoRequest));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public AlbumResponse update(@PathVariable("id") Long id, @RequestBody AlbumDtoRequest albumDtoRequest) {
        albumDtoRequest.setId(id);
        return build(albumService.update(albumDtoRequest));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") Long id) {
        albumService.delete(id);
    }

    private AlbumResponse build(Album a) {
        AlbumResponse albumResponse = new AlbumResponse(
                a.getId(),
                a.getName(),
                a.getGenre().getId()
        );

        if (!CollectionUtils.isEmpty(a.getTracks())) {
            albumResponse.setTrackIds(a.getTracks().stream().map(Track::getId).collect(Collectors.toList()));
        }

        return albumResponse;
    }

    private List<AlbumResponse> buildList(List<Album> list) {
        List<AlbumResponse> responseList = new ArrayList<>(list.size());
        list.forEach((a) -> responseList.add(build(a)));

        return responseList;
    }
}

