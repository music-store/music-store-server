package com.music.demo.repository;

import com.music.demo.domain.PlayList;
import com.music.demo.domain.Track;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface PlayListRepository extends PagingAndSortingRepository<PlayList, Long> {
    List<PlayList> findAllByTracksIsContaining(Track track);

    List<PlayList> findAllByUserId(Integer userId);

    void deleteById(Long id);
}
