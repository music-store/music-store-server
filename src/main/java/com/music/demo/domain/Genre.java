package com.music.demo.domain;

import javax.persistence.*;

@Entity
@Table(name = "GENRES")
public class Genre extends BaseEntity<Integer> {
    @Column(name = "NAME", nullable = false)
    private String name;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "genre")
    private Album album;

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
