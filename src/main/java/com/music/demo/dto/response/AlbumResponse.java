package com.music.demo.dto.response;

import java.util.List;

public class AlbumResponse {
    private Long id;
    private String name;
    private Integer genreId;
    private List<Long> trackIds;

    public AlbumResponse(Long id, String name, Integer genreId) {
        this.id = id;
        this.name = name;
        this.genreId = genreId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getGenreId() {
        return genreId;
    }

    public void setGenreId(Integer genreId) {
        this.genreId = genreId;
    }

    public List<Long> getTrackIds() {
        return trackIds;
    }

    public void setTrackIds(List<Long> trackIds) {
        this.trackIds = trackIds;
    }
}
