package com.music.demo.repository;

import com.music.demo.domain.Track;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TrackRepository extends PagingAndSortingRepository<Track, Long> {

    List<Track> findAllByAlbum_Id(Long id);

    void deleteAllByAlbum_Id(Long id);
}
